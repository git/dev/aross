# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit zproduct

MY_PV=${PV/_/-}
PV_NEW=${MY_PV/./_}
DESCRIPTION="LDAP User Authentication for Zope"
HOMEPAGE="http://www.dataflake.org/software/ldapuserfolder/"
SRC_URI="${HOMEPAGE}/${PN}_${PV/_/-}/LDAPUserFolder-${PV_NEW}.tgz"

LICENSE="ZPL"
KEYWORDS="~ppc ~sparc ~x86"

RDEPEND=">=dev-python/python-ldap-2.0.6"

ZPROD_LIST="LDAPUserFolder"
