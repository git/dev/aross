# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit linux-info eutils multilib toolchain-funcs

DESCRIPTION="Layer 2 tunneling protocol network server (LNS)"
HOMEPAGE="http://sourceforge.net/projects/l2tpns"
SRC_URI="mirror://sourceforge/${PN}/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=">=dev-libs/libcli-1.8.5"
RDEPEND="${DEPEND}"

CONFIG_CHECK="TUN"

src_unpack() {
	unpack ${A}

	pushd "${S}"
	epatch "${FILESDIR}/${PN}-trust-localhost.patch"

	# Without "#include <linux/types.h>", compilation fails with:
	# l2tpns.c:509: error: syntax error before "__u32"
	sed -i  \
		-e '/^#include <libcli.h>$/ a #include <linux/types.h>' \
		"${S}"/l2tpns.c || die "sed on l2tpns.c failed"
}

src_compile() {
	emake \
		OPTIM="${CFLAGS}" \
		LDFLAGS="${LDFLAGS}" \
		CC="$(tc-getCC)" \
		LD="$(tc-getCC)" \
		libdir="/usr/$(get_libdir)/${PN}" \
		|| die "emake failed"
}

src_install() {
	emake install \
		DESTDIR="${D}" \
		libdir="/usr/$(get_libdir)/${PN}" \
		|| die "emake install failed"

	dodoc THANKS Changes
	dohtml Docs/manual.html
}
